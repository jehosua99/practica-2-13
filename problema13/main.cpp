#include <iostream>
/*
 La fotografia digitalizada es una matriz de 6x8; el ususario debera ingresar la posicion "i" y "j" y se le dira si en ella hay una
 estrella
 */
using namespace std;

int posibleEstrella(int, int, int [][8]);
int main()
{
    int numEstrellas = 0, fila, columna, fotografia[][8] = {{0, 3, 4, 0, 0, 0, 6, 8}, {5, 13, 6, 0, 0, 0, 2, 3}, {2, 6, 2, 7, 3, 0, 10, 0}, {0, 0, 4, 15, 4, 1, 6, 0}, {0, 0, 7, 12, 6, 9, 10, 4}, {5, 0, 6, 10, 6, 4, 8, 0}};
    for (fila = 1; fila <= 4; fila ++)
    {
        for (columna = 1; columna <= 6; columna ++)
        {
            if (posibleEstrella(fila, columna, fotografia) > 6)  //Verifico que sea una estrella si cumple la condicion que se nos da
            {
                cout << "En la posicion " << fila + 1 << ", " << columna + 1 << " hay una estrella\n";  //Sumo 1 para facilidad del usuario que por lo general se empezaria a contar desde 1
                numEstrellas += 1;
            }
        }
    }
    cout << "En total en la imagen hay: " << numEstrellas << " estrellas.\n";
    return 0;
}

int posibleEstrella(int i, int j, int imagen[][8]) //Funcion que calculara el valor de la operacion que se nos da
{
    int valor;
    int (*p)[8] = imagen;  //Creo el puntero que apuntara a la fotografia
    valor = ((*(*(p+i)+j)) + (*(*(p+i)+(j-1))) + (*(*(p+i)+(j+1))) + (*(*(p+(i-1))+j)) + (*(*(p+(i+1))+j)))/5;  //Me muevo con el puntero por la matriz siguiendo las instrucciones de la formula dada
    return valor;
}
